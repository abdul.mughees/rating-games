defmodule RatingApp.Repo do
  use Ecto.Repo,
    otp_app: :rating_app,
    adapter: Ecto.Adapters.Postgres
end
