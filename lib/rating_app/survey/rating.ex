defmodule RatingApp.Survey.Rating do
  use Ecto.Schema
  import Ecto.Changeset

  schema "ratings" do
    field :rating, :integer

    belongs_to :game, RatingApp.Catalog.Game
    belongs_to :user, RatingApp.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(rating, attrs) do
    rating
    |> cast(attrs, [:rating, :game_id, :user_id])
    |> validate_required([:rating, :game_id, :user_id])
    |> unique_constraint(:unique_game_user_constraint, name: :ratings_user_id_game_id_index)
  end
end
