defmodule RatingAppWeb.RatingLive.Index do
  use RatingAppWeb, :live_view

  alias RatingApp.Survey
  alias RatingApp.Survey.Rating
  alias RatingApp.Catalog
  alias RatingApp.Accounts

  @impl true
  def mount(_params, %{"user_token" => user_token}, socket) do
    current_user = Accounts.get_user_by_session_token(user_token)

    socket =
      socket
      |> assign(:current_user, current_user)
      |> assign(:ratings, list_ratings())
      |> assign(:games, Catalog.list_games())

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Rating")
    |> assign(:rating, Survey.get_rating!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Rating")
    |> assign(:rating, %Rating{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Ratings")
    |> assign(:rating, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    rating = Survey.get_rating!(id)
    {:ok, _} = Survey.delete_rating(rating)

    {:noreply, assign(socket, :ratings, list_ratings())}
  end

  defp list_ratings do
    Survey.list_ratings()
  end
end
