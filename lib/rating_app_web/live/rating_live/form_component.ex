defmodule RatingAppWeb.RatingLive.FormComponent do
  use RatingAppWeb, :live_component

  alias RatingApp.Survey

  @impl true
  def update(%{rating: rating} = assigns, socket) do
    changeset = Survey.change_rating(rating)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"rating" => rating_params}, socket) do
    changeset =
      socket.assigns.rating
      |> Survey.change_rating(rating_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"rating" => rating_params}, socket) do
    rating = Map.put(rating_params, "user_id", socket.assigns.current_user.id)
    save_rating(socket, socket.assigns.action, rating)
  end

  defp save_rating(socket, :edit, rating_params) do
    case Survey.update_rating(socket.assigns.rating, rating_params) do
      {:ok, _rating} ->
        {:noreply,
         socket
         |> put_flash(:info, "Rating updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_rating(socket, :new, rating_params) do
    case Survey.create_rating(rating_params) do
      {:ok, _rating} ->
        {:noreply,
         socket
         |> put_flash(:info, "Rating created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
