defmodule RatingAppWeb.PageController do
  use RatingAppWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
