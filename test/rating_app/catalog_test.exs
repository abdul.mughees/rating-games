defmodule RatingApp.CatalogTest do
  use RatingApp.DataCase

  alias RatingApp.Catalog

  describe "game" do
    alias RatingApp.Catalog.Game

    import RatingApp.CatalogFixtures

    @invalid_attrs %{game_name: nil}

    test "list_game/0 returns all game" do
      game = game_fixture()
      assert Catalog.list_game() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert Catalog.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      valid_attrs = %{game_name: "some game_name"}

      assert {:ok, %Game{} = game} = Catalog.create_game(valid_attrs)
      assert game.game_name == "some game_name"
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalog.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      update_attrs = %{game_name: "some updated game_name"}

      assert {:ok, %Game{} = game} = Catalog.update_game(game, update_attrs)
      assert game.game_name == "some updated game_name"
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalog.update_game(game, @invalid_attrs)
      assert game == Catalog.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = Catalog.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> Catalog.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = Catalog.change_game(game)
    end
  end

  describe "game" do
    alias RatingApp.Catalog.Games

    import RatingApp.CatalogFixtures

    @invalid_attrs %{game_name: nil}

    test "list_game/0 returns all game" do
      games = games_fixture()
      assert Catalog.list_game() == [games]
    end

    test "get_games!/1 returns the games with given id" do
      games = games_fixture()
      assert Catalog.get_games!(games.id) == games
    end

    test "create_games/1 with valid data creates a games" do
      valid_attrs = %{game_name: "some game_name"}

      assert {:ok, %Games{} = games} = Catalog.create_games(valid_attrs)
      assert games.game_name == "some game_name"
    end

    test "create_games/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalog.create_games(@invalid_attrs)
    end

    test "update_games/2 with valid data updates the games" do
      games = games_fixture()
      update_attrs = %{game_name: "some updated game_name"}

      assert {:ok, %Games{} = games} = Catalog.update_games(games, update_attrs)
      assert games.game_name == "some updated game_name"
    end

    test "update_games/2 with invalid data returns error changeset" do
      games = games_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalog.update_games(games, @invalid_attrs)
      assert games == Catalog.get_games!(games.id)
    end

    test "delete_games/1 deletes the games" do
      games = games_fixture()
      assert {:ok, %Games{}} = Catalog.delete_games(games)
      assert_raise Ecto.NoResultsError, fn -> Catalog.get_games!(games.id) end
    end

    test "change_games/1 returns a games changeset" do
      games = games_fixture()
      assert %Ecto.Changeset{} = Catalog.change_games(games)
    end
  end

  describe "games" do
    alias RatingApp.Catalog.Game

    import RatingApp.CatalogFixtures

    @invalid_attrs %{game_name: nil}

    test "list_games/0 returns all games" do
      game = game_fixture()
      assert Catalog.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert Catalog.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      valid_attrs = %{game_name: "some game_name"}

      assert {:ok, %Game{} = game} = Catalog.create_game(valid_attrs)
      assert game.game_name == "some game_name"
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalog.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      update_attrs = %{game_name: "some updated game_name"}

      assert {:ok, %Game{} = game} = Catalog.update_game(game, update_attrs)
      assert game.game_name == "some updated game_name"
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalog.update_game(game, @invalid_attrs)
      assert game == Catalog.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = Catalog.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> Catalog.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = Catalog.change_game(game)
    end
  end
end
