defmodule RatingAppWeb.GamesLiveTest do
  use RatingAppWeb.ConnCase

  import Phoenix.LiveViewTest
  import RatingApp.CatalogFixtures

  @create_attrs %{game_name: "some game_name"}
  @update_attrs %{game_name: "some updated game_name"}
  @invalid_attrs %{game_name: nil}

  defp create_games(_) do
    games = games_fixture()
    %{games: games}
  end

  describe "Index" do
    setup [:create_games]

    test "lists all game", %{conn: conn, games: games} do
      {:ok, _index_live, html} = live(conn, Routes.games_index_path(conn, :index))

      assert html =~ "Listing Game"
      assert html =~ games.game_name
    end

    test "saves new games", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.games_index_path(conn, :index))

      assert index_live |> element("a", "New Games") |> render_click() =~
               "New Games"

      assert_patch(index_live, Routes.games_index_path(conn, :new))

      assert index_live
             |> form("#games-form", games: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#games-form", games: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.games_index_path(conn, :index))

      assert html =~ "Games created successfully"
      assert html =~ "some game_name"
    end

    test "updates games in listing", %{conn: conn, games: games} do
      {:ok, index_live, _html} = live(conn, Routes.games_index_path(conn, :index))

      assert index_live |> element("#games-#{games.id} a", "Edit") |> render_click() =~
               "Edit Games"

      assert_patch(index_live, Routes.games_index_path(conn, :edit, games))

      assert index_live
             |> form("#games-form", games: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#games-form", games: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.games_index_path(conn, :index))

      assert html =~ "Games updated successfully"
      assert html =~ "some updated game_name"
    end

    test "deletes games in listing", %{conn: conn, games: games} do
      {:ok, index_live, _html} = live(conn, Routes.games_index_path(conn, :index))

      assert index_live |> element("#games-#{games.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#games-#{games.id}")
    end
  end

  describe "Show" do
    setup [:create_games]

    test "displays games", %{conn: conn, games: games} do
      {:ok, _show_live, html} = live(conn, Routes.games_show_path(conn, :show, games))

      assert html =~ "Show Games"
      assert html =~ games.game_name
    end

    test "updates games within modal", %{conn: conn, games: games} do
      {:ok, show_live, _html} = live(conn, Routes.games_show_path(conn, :show, games))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Games"

      assert_patch(show_live, Routes.games_show_path(conn, :edit, games))

      assert show_live
             |> form("#games-form", games: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#games-form", games: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.games_show_path(conn, :show, games))

      assert html =~ "Games updated successfully"
      assert html =~ "some updated game_name"
    end
  end
end
