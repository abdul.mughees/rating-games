defmodule RatingApp.CatalogFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `RatingApp.Catalog` context.
  """

  @doc """
  Generate a game.
  """
  def game_fixture(attrs \\ %{}) do
    {:ok, game} =
      attrs
      |> Enum.into(%{
        game_name: "some game_name"
      })
      |> RatingApp.Catalog.create_game()

    game
  end

  @doc """
  Generate a games.
  """
  def games_fixture(attrs \\ %{}) do
    {:ok, games} =
      attrs
      |> Enum.into(%{
        game_name: "some game_name"
      })
      |> RatingApp.Catalog.create_games()

    games
  end
end
