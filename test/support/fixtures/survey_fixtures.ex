defmodule RatingApp.SurveyFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `RatingApp.Survey` context.
  """

  @doc """
  Generate a rating.
  """
  def rating_fixture(attrs \\ %{}) do
    {:ok, rating} =
      attrs
      |> Enum.into(%{
        rating: 42
      })
      |> RatingApp.Survey.create_rating()

    rating
  end
end
